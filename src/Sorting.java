import java.io.IOException;
import java.io.File;
import java.io.FileWriter;
import java.util.Comparator;
import java.util.Scanner;
import java.util.PriorityQueue;

// Структура данных с строкой/числом из файла и индексом файла
class Node<T extends Comparable<T>> {
    T data;
    int index;
    Node(T obj, int i) {
        data = obj;
        index = i;
    }
}
// Переопределение компаратора для сортировки Node по возрастанию
class NodeAscending implements Comparator<Node> {
    public int compare (Node a, Node b) {
        return a.data.compareTo(b.data);
    }
}
// Переопределение компаратора для сортировки Node по убыванию
class NodeDescending implements Comparator<Node> {
    public int compare (Node a, Node b) {
        return b.data.compareTo(a.data);
    }
}
public class Sorting {

	public static void main(String[] args) throws IOException {
// Инициализация
        FileWriter out = null;
        Scanner[] files_mass = null;
        String type_key = "", order_key = "";

// Парсинг ключей
        for (int i = 0; i < 2; i++) {
            if (args[i].equals("-i") || args[i].equals("-s")) {
                type_key = args[i];
            }
            if (args[i].equals("-a") || args[i].equals("-d")) {
                order_key = args[i];
            }
        }

// Парсинг файлов
        if (order_key.equals("")) {
            
            files_mass = new Scanner[args.length - 2];
            for (int i = 2; i < args.length; i++) {
                try {
                    files_mass[i-2] = new Scanner(new File(args[i]));
                } catch (IOException e) {
                    System.out.println("File: "+args[i]+" can't be scanned");
                    throw new IOException(e);
                }
            }
            try {
                out = new FileWriter(args[1]);
            } catch (Exception e) {
                System.out.println("output file can't be opened");
                throw new IOException(e);
            }
        }
        else {
            files_mass = new Scanner[args.length - 3];
            for (int i = 3; i < args.length; i++) {
                try {
                    files_mass[i-3] = new Scanner(new File(args[i]));
                } catch (IOException e) {
                    System.out.println("File: "+args[i]+" can't be scanned");
                    throw new IOException(e);
                }
            }
            try {
                out = new FileWriter(args[2]);
            } catch (Exception e) {
                System.out.println("Output file can't be opened");
                throw new IOException(e);
            }
        }
// Объявление и инициализация компаратора       
        Comparator<Node> cmp;
        if (order_key.equals("-d")) {
            cmp = new NodeDescending();
        }
        else {
            cmp = new NodeAscending();
        }
// Запуск сортировки по ключам
        if (type_key.equals("-i")) {
            merge_files_int(out, files_mass, cmp);
        }
        else {
            merge_files_string(out, files_mass, cmp);
        }
// Закрытие файлов
        for (int i = 0; i < files_mass.length; i++) {
			files_mass[i].close();
		}
        try {
            out.close();
        } catch (Exception e) {
            System.out.println("Output file can't be closed");
            throw new IOException(e);
        }

	}

// Функция для сортировки числовых фалов
    public static void merge_files_int(FileWriter out, Scanner[] files, Comparator<Node> cmp) throws IOException {
        PriorityQueue<Node<Integer>> Heap = new PriorityQueue<Node<Integer>>(cmp);
        for (int i = 0; i < files.length; i++) {
            if (files[i].hasNextInt()) {
                Heap.add(new Node(files[i].nextInt(), i));
            }
        }
        Node<Integer> prev;
        Node<Integer> current;
        while (!Heap.isEmpty()) {
            prev = Heap.remove();
            out.write(Integer.toString(prev.data) + "\n");
            if (files[prev.index].hasNextInt()) {
                current = new Node(files[prev.index].nextInt(), prev.index);
                if (cmp.compare(prev, current) > 0) {
                   System.out.println("Files are not sorted");
                   return;
                }
                Heap.add(current);
            }
        }
	}
// Функция для сортировки строковых фалов
    public static void merge_files_string(FileWriter out, Scanner[] files, Comparator<Node> cmp) throws IOException {
        PriorityQueue<Node<String>> Heap = new PriorityQueue<Node<String>>(cmp);
        for (int i = 0; i < files.length; i++) {
            if (files[i].hasNextLine()) {
                String data = files[i].nextLine();
                if (data.indexOf(" ") >= 0) {
                    System.out.println("The string should not contain spaces");
                    return;
                }
                Heap.add(new Node(data, i));
            }
        }
        Node<String> prev;
        Node<String> current;
        while (!Heap.isEmpty()) {
            prev = Heap.remove();
            out.write(prev.data + "\n");
            if (files[prev.index].hasNextLine()) {
                current = new Node(files[prev.index].nextLine(), prev.index);
                if (current.data.indexOf(" ") >= 0) {
                    System.out.println("The string should not contain spaces");
                    return;
                }
                if (cmp.compare(prev, current) > 0) {
                   System.out.println("Files are not sorted"); 
                   return;
                }
                Heap.add(current);
            }
        }
	}
}